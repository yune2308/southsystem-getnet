import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

@DisplayName("Testes")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestApi {

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://reqres.in/api/";
    }

    @Test
    @Order(1)
    @DisplayName("Usuário obtido com sucesso")
    void getSingleUserSuccessful() {
        given().contentType(ContentType.JSON)
                .when()
                .get("/users/{id}", 2)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("data.first_name", equalTo("Janet"))
                .body("data.last_name", equalTo("Weaver"));
    }

    @Test
    @Order(2)
    @DisplayName("Usuário não encontrado")
    public void getSingleUserNotFound() {
        given().contentType(ContentType.JSON)
                .when()
                .get("/users/{id}", 23)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    @Order(3)
    @DisplayName("Usuário criado")
    public void createUser() {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("name", "Yunelis");
        requestBody.put("job", "QA");

        given().contentType(ContentType.JSON)
                .and()
                .body(requestBody)
                .when()
                .post("/users")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .body("name", equalTo("Yunelis"));
    }

    @Test
    @Order(4)
    @DisplayName("Usuário deletado")
    public void deleteUser() {
        given().contentType(ContentType.JSON)
                .when()
                .delete("/users/{id}", 2)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    @Order(5)
    @DisplayName("Registrado com sucesso")
    public void registerSuccessful() {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("email", "eve.holt@reqres.in");
        requestBody.put("password", "pistol");

        given().contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post("/register")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("token", notNullValue());
    }

    @Test
    @Order(6)
    @DisplayName("Registrado com erro")
    public void registerUnsuccessful() {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("email", "sydney@fife");

        given().contentType(ContentType.JSON)
                .and()
                .body(requestBody)
                .when()
                .post("/register")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("error", equalTo("Missing password"));
    }

    @Test
    @Order(7)
    @DisplayName("Usuário obtido com atraso")
    public void getDelay() {
        given().contentType(ContentType.JSON)
                .and()
                .param("delay", 3)
                .when()
                .get("/users")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    @Order(8)
    @DisplayName("Obter toda a página")
    void getAllPage() {
        given().contentType(ContentType.JSON)
                .param("page", 2)
                .when()
                .get("/users")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("data.id[0]", equalTo(7))
                .body("data.first_name", hasItems("Byron", "Rachel"));
    }

    @Test
    @Order(9)
    @DisplayName("Usuário atualizado com sucesso")
    public void updateUser() {

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("job", "Qualidade");

        given().contentType(ContentType.JSON)
                .and()
                .body(requestBody)
                .when()
                .put("/users/{id}", 2)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("job", equalTo("Qualidade"));
    }
}
